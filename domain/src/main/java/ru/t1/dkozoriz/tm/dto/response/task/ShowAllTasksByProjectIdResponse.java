package ru.t1.dkozoriz.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ShowAllTasksByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> taskList;

}