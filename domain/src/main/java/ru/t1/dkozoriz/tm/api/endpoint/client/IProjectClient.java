package ru.t1.dkozoriz.tm.api.endpoint.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.response.project.*;

public interface IProjectClient extends IEndpointClient {

    @NotNull
    ShowProjectListResponse list(@NotNull ProjectShowListRequest request);

    @NotNull
    ChangeProjectStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ChangeProjectStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    CompleteProjectByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    CompleteProjectByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    StartProjectByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    StartProjectByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    CreateProjectResponse projectCreate(@NotNull ProjectCreateRequest request);

    @NotNull
    ListClearProjectResponse projectListClear(@NotNull ProjectListClearRequest request);

    @NotNull
    RemoveProjectByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    RemoveProjectByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ShowProjectByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    ShowProjectByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    UpdateProjectByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    UpdateProjectByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}