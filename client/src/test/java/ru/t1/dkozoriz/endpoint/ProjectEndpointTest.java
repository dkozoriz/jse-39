package ru.t1.dkozoriz.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.SoapCategory;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.ITokenService;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserRemoveRequest;
import ru.t1.dkozoriz.tm.dto.response.project.*;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.service.PropertyService;
import ru.t1.dkozoriz.tm.service.TokenService;

import java.util.ArrayList;
import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final List<Project> projectList = new ArrayList<>();

    @BeforeClass
    public static void setConnection() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("test_user", "test", null);
        USER_ENDPOINT.userRegistry(registryRequest);
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("test_user", "test");
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE.setToken(adminToken);
    }

    @Before
    public void initTest() {
        final int numberOfProjects = 10;
        for (int i = 1; i <= numberOfProjects; i++) {
            @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                    TOKEN_SERVICE.getToken(),
                    "Project " + i,
                    "Description " + i
            );
            @Nullable final Project project = PROJECT_ENDPOINT.projectCreate(request).getProject();
            Assert.assertNotNull(project);
            projectList.add(project);
        }
    }

    @Test
    public void testShowList() {
        @NotNull final ProjectShowListRequest request = new ProjectShowListRequest(TOKEN_SERVICE.getToken(), null);
        @Nullable final ShowProjectListResponse response = PROJECT_ENDPOINT.projectList(request);
        @NotNull List<Project> list = response.getProjectList();
        Assert.assertEquals(projectList.size(), list.size());
    }

    @Test
    public void testShowProjectById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(TOKEN_SERVICE.getToken(), project.getId());
            @Nullable final Project currentProject = PROJECT_ENDPOINT.projectShowById(request).getProject();
            Assert.assertNotNull(currentProject);
            Assert.assertEquals(currentProject.getId(), project.getId());
        }
    }

    @Test
    public void testShowProjectByIndex() {
        for (int i = 1; i < projectList.size(); i++) {
            @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(TOKEN_SERVICE.getToken(), i);
            @Nullable final Project currentProject = PROJECT_ENDPOINT.projectShowByIndex(request).getProject();
            Assert.assertNotNull(currentProject);
            Assert.assertEquals(currentProject.getName(), projectList.get(i).getName());
        }
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                TOKEN_SERVICE.getToken(), projectId, status
        );
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectChangeStatusById(request).getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(status, currentProject.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final Integer projectIndex = 0;
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                TOKEN_SERVICE.getToken(), projectIndex, status
        );
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectChangeStatusByIndex(request).getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(status, currentProject.getStatus());
    }

    @Test
    public void testCompleteProjectById() {
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(
                TOKEN_SERVICE.getToken(), projectId
        );
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectCompleteById(request).getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(Status.COMPLETED, currentProject.getStatus());
    }

    @Test
    public void testCompleteProjectByIndex() {
        @NotNull final Integer projectIndex = 0;
        @NotNull final ProjectCompleteByIndexRequest request
                = new ProjectCompleteByIndexRequest(TOKEN_SERVICE.getToken(), projectIndex);
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectCompleteByIndex(request).getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(Status.COMPLETED, currentProject.getStatus());
    }

    @Test
    public void testStartProjectById() {
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(
                TOKEN_SERVICE.getToken(), projectId
        );
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectStartById(request).getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(Status.IN_PROGRESS, currentProject.getStatus());
    }

    @Test
    public void testStartProjectByIndex() {
        @NotNull final Integer projectIndex = 0;
        @NotNull final ProjectStartByIndexRequest request
                = new ProjectStartByIndexRequest(TOKEN_SERVICE.getToken(), projectIndex);
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectStartByIndex(request).getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(Status.IN_PROGRESS, currentProject.getStatus());
    }

    @Test
    public void testProjectCreate() {
        @NotNull final String name = "Test name";
        @NotNull final String description = "Test description";
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(TOKEN_SERVICE.getToken(), name, description);
        @NotNull final CreateProjectResponse response = PROJECT_ENDPOINT.projectCreate(request);
        @Nullable final Project currentProject = response.getProject();
        Assert.assertNotNull(currentProject);
        Assert.assertEquals(currentProject.getName(), name);
        Assert.assertEquals(currentProject.getDescription(), description);
    }

    @Test
    public void testClearProjectList() {
        @NotNull final ProjectListClearRequest clearRequest = new ProjectListClearRequest(TOKEN_SERVICE.getToken());
        PROJECT_ENDPOINT.projectListClear(clearRequest);
        @NotNull final ProjectShowListRequest showRequest = new ProjectShowListRequest(TOKEN_SERVICE.getToken(), null);
        @NotNull final ShowProjectListResponse showResponse = PROJECT_ENDPOINT.projectList(showRequest);
        @Nullable final List<Project> currentProjectList = showResponse.getProjectList();
        Assert.assertNull(currentProjectList);
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final int oldSize = projectList.size();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(TOKEN_SERVICE.getToken(), projectId);
        PROJECT_ENDPOINT.projectRemoveById(request);
        @NotNull final ProjectShowListRequest showRequest = new ProjectShowListRequest(TOKEN_SERVICE.getToken(), null);
        @Nullable final ShowProjectListResponse response = PROJECT_ENDPOINT.projectList(showRequest);
        @NotNull List<Project> currentList = response.getProjectList();
        Assert.assertEquals(currentList.size(), oldSize-1);
    }

    @Test
    public void testRemoveProjectByIndex() {
        @NotNull final Integer projectIndex = 0;
        @NotNull final int oldSize = projectList.size();
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(TOKEN_SERVICE.getToken(), projectIndex);
        PROJECT_ENDPOINT.projectRemoveByIndex(request);
        @NotNull final ProjectShowListRequest showRequest = new ProjectShowListRequest(TOKEN_SERVICE.getToken(), null);
        @Nullable final ShowProjectListResponse response = PROJECT_ENDPOINT.projectList(showRequest);
        @NotNull List<Project> currentList = response.getProjectList();
        Assert.assertEquals(currentList.size(), oldSize-1);
    }

    @Test
    public void testUpdateProjectById() {
            @NotNull final String name = "new Project";
            @NotNull final String description = "new Description";
            @NotNull final String projectId = projectList.get(0).getId();
            @NotNull final ProjectUpdateByIdRequest request
                    = new ProjectUpdateByIdRequest(TOKEN_SERVICE.getToken(), projectId, name, description);
            @Nullable final Project currentProject = PROJECT_ENDPOINT.projectUpdateById(request).getProject();
            Assert.assertEquals(currentProject.getName(), name);
            Assert.assertEquals(currentProject.getDescription(), description);
    }

    @Test
    public void testUpdateProjectByIndex() {
        @NotNull final String name = "new Project";
        @NotNull final String description = "new Description";
        @NotNull final Integer projectIndex = 0;
        @NotNull final ProjectUpdateByIndexRequest request
                = new ProjectUpdateByIndexRequest(TOKEN_SERVICE.getToken(), projectIndex, name, description);
        @Nullable final Project currentProject = PROJECT_ENDPOINT.projectUpdateByIndex(request).getProject();
        Assert.assertEquals(currentProject.getName(), name);
        Assert.assertEquals(currentProject.getDescription(), description);
    }

    @After
    public void after() {
        @NotNull final ProjectListClearRequest request = new ProjectListClearRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(PROJECT_ENDPOINT.projectListClear(request));
    }

    @AfterClass
    public static void clearUser() {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest("admin", "admin");
        @Nullable final String token = AUTH_ENDPOINT.login(requestLogin).getToken();
        Assert.assertNotNull(token);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(token, "test_user");
        USER_ENDPOINT.userRemove(removeRequest);
    }

}