package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    public TaskStartByIdCommand() {
        super("task-start-by-id", "start task by id.");
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getEndpointLocator().getTaskEndpoint().taskStartById(new TaskStartByIdRequest(getToken(), id));
    }

}