package ru.t1.dkozoriz.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert(
            "INSERT INTO tm_user (row_id, login, password, locked, first_name, middle_name, last_name, email, role) " +
                    "VALUES (#{id}, #{login}, #{passwordHash}, #{locked},  #{firstName}, #{middleName}, #{lastName}, " +
                    " #{email}, #{role})"
    )
    void add(@NotNull User user);

    @Update(
            "UPDATE tm_user SET locked = #{locked}, first_name = #{firstName}, middle_name = #{middleName} " +
                    ", last_name = #{lastName}, password = #{passwordHash}, role = #{role} WHERE row_id = #{id}"
    )
    void update(@NotNull User user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Nullable
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "passwordHash", column = "password")
    })
    List<User> findAll();


    @Nullable
    @Select("SELECT * FROM tm_user WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "locked", column = "locked_flg"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "passwordHash", column = "password")
    })
    User findById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_user WHERE row_id = #{id}")
    void remove(@NotNull User user);

    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize();

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findByLogin(@Nullable @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findByEmail(@Nullable @Param("email") String email);

}