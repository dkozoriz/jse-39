package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable String projectId
    ) ;

    int getSize(@Nullable String userId);

    List<Task> findAll(@Nullable String userId);

    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    List<Task> findAll(@Nullable String userId, @Nullable Comparator comparator);

    Task findById(@Nullable String userId, @Nullable String id);

    Task findByIndex(@Nullable String userId, @Nullable Integer index);

    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    public Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    Task changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    Task changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}