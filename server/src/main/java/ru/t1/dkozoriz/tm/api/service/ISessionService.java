package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.Session;

import java.util.List;

public interface ISessionService {

    void add(@NotNull Session session);

    void clear(@Nullable String userId);

    void clear();

    void remove(@Nullable Session session);

    List<Session> findAll(@Nullable String userId);

    Session findById(@Nullable String id);

}