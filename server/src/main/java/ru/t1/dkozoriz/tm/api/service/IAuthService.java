package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String Email) throws Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session) throws Exception;

}