package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    int getSize(@Nullable String userId);

    List<Project> findAll(@Nullable String userId);

    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    List<Project> findAll(@Nullable String userId, @Nullable Comparator comparator);

    Project findById(@Nullable String userId, @Nullable String id);

    Project findByIndex(@Nullable String userId, @Nullable Integer index);

    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    public Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    Project changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    Project changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

}